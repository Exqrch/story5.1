from django import forms

class FormMatakuliah(forms.Form):
	nama_matkul = forms.CharField(max_length=40, required=True)
	dosen 		= forms.CharField(max_length=40, required=True)
	sks 		= forms.IntegerField(required=True)
	ruang 		= forms.CharField(max_length=8, required=True)
	tahun_ajaran= forms.CharField(max_length=16, required=True)
	deskripsi 	= forms.CharField(widget=forms.Textarea)