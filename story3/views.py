from django.shortcuts import render

def story3index(request) :
	return render(request,'story3/story3index.html')

def story3hobbies(request) :
	return render(request,'story3/story3hobbies.html')

def story3org(request) :
	return render(request,'story3/story3org.html')

def story3achievement(request) :
	return render(request,'story3/story3achievement.html')